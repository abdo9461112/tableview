//
//  countryViewController.swift
//  table view
//
//  Created by abdo emad  on 17/06/2023.
//

import UIKit

class countryViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var arrCountries = ["Saudi Arabia", "Oman", "Egypt"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        
        arrCountries.removeAll()
        if arrCountries.count == 0{
            tableView.isHidden = true
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrCountries.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Zzzzzz", for: indexPath)
        cell.textLabel?.text = arrCountries[indexPath.row]
        return cell
    }
}
