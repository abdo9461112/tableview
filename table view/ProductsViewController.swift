//
//  ProductsViewController.swift
//  table view
//
//  Created by abdo emad  on 03/02/2024.
//

import UIKit

class ProductsViewController: UIViewController , UITextFieldDelegate{

    var selectedFruit : Fruit!
    var item = [Fruit]()
    
    @IBOutlet weak var descrpitonField: UITextField!
    @IBOutlet weak var priceFiield: UITextField!
    @IBOutlet weak var ProductField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        descrpitonField.delegate = self
        priceFiield.delegate = self
        ProductField.delegate = self
        
        ProductField.text = selectedFruit.name
        priceFiield.text = ("\(selectedFruit.price)")
        descrpitonField.text = selectedFruit.ddd
        navigationItem.title = selectedFruit.name
      
        }
    
    override func viewWillAppear(_ animated: Bool) {
        ProductField.becomeFirstResponder()
    }
    
  
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == ProductField{
            priceFiield.becomeFirstResponder()
        }else if textField == priceFiield{
            descrpitonField.becomeFirstResponder()
        }else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}
