//
//  HomeViewController.swift
//  table view
//
//  Created by abdo emad  on 11/06/2023.
//

import UIKit

class HomeViewController : UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var arrFruits = [Fruit]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        

        navigationItem.title = "Products"
        
        arrFruits.append(Fruit.init(name: "Apple", price: 12, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "apple")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Banana", price: 9, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "banana")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Lemon", price: 2, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "lemon")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Mango", price: 39, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "mango")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Orange", price: 49, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "orange")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Papaya", price: 50, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "papaya")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Apple", price: 12, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "apple")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Banana", price: 9, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "banana")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Lemon", price: 2, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "lemon")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Mango", price: 39, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "mango")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Orange", price: 49, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "orange")!, done: false))
        
        arrFruits.append(Fruit.init(name: "Papaya", price: 50, ddd: "This is a populer fruit for people of all ages", photo: UIImage(named: "papaya")!, done: false))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFruits.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homecell") as! HomeTableViewCell
        let data = arrFruits[indexPath.row]
        cell.setupCell(photo: data.photo, name: data.name, ddd: data.ddd, price: data.price)
        cell.butnAdd.tag = indexPath.row
        
        cell.butnAdd.addTarget(self, action: #selector(addToFavourite(sender:)), for: .touchUpInside)
        
        if data.done == true{
            cell.butnAdd.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }else{
            cell.butnAdd.setImage(UIImage(systemName: "heart"), for: .normal)
            
        }
        
        return cell
    }
    @objc func addToFavourite(sender : UIButton){
        // Use the updated state to set the heart button image
        if arrFruits[sender.tag].done == false {
            sender.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            arrFruits[sender.tag].done = true
            print(arrFruits[sender.tag].name)
        } else {
            sender.setImage(UIImage(systemName: "heart"), for: .normal)
            arrFruits[sender.tag].done = false
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "prodcut" {
            let row = tableView.indexPathForSelectedRow?.row
            let item = arrFruits[row!]
            let destination = segue.destination as! ProductsViewController
            destination.selectedFruit = item
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}


struct Fruit {
    var name : String
    var price : Double
    var ddd : String
    let photo : UIImage
    var done : Bool = false
    
}


