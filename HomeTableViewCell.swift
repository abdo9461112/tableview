//
//  HomeTableViewCell.swift
//  table view
//
//  Created by abdo emad  on 11/06/2023.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgFruit: UIImageView!
    @IBOutlet weak var nameFruit: UILabel!
    @IBOutlet weak var priceFruit: UILabel!
    @IBOutlet weak var dddFruit: UILabel!
    @IBOutlet weak var butnAdd: UIButton!
    

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func setupCell(photo : UIImage , name : String , ddd : String , price : Double){
        
        imgFruit.image = photo
        nameFruit.text = name
        dddFruit.text = ddd
        priceFruit.text = "\(price) Eg"

    }
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
