//
//  connectViewController.swift
//  table view
//
//  Created by abdo emad  on 15/06/2023.
//

import UIKit

class connectViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
  
    
    @IBOutlet weak var contactsLableCount: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrSections = ["A", "m", "h"]
    
    var arrRows = [["Ali","Abdo","Ahmed","aya","Ali","Abdo","Ahmed","aya"],
                   ["Mohamed","Menna","Mohamed","Menna","Mohamed","Menna"],
                   ["Halla","Hassan","Hanan","Halla","Hassan","Hanan","Halla","Hassan","Hanan"]]
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
    
        let totalRowsCount = arrRows.flatMap { $0 }.count
        contactsLableCount.text = String("\(totalRowsCount) Contacts")
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSections.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrRows[section].count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ccell", for: indexPath)
        cell.textLabel?.text = arrRows[indexPath.section][indexPath.row]

        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     return arrSections[section]
    }
    

    
}
