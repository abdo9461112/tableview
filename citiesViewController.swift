//
//  citiesViewController.swift
//  table view
//
//  Created by abdo emad  on 17/06/2023.
//

import UIKit

class citiesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    
    @IBOutlet weak var tableView: UITableView!
    
    var arrcities = [UIImage(named: "img_amman")!,UIImage(named: "img_dubai 1")!,UIImage(named: "img_cairo")!,UIImage(named: "img_riyadh")!]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrcities.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "city") as! cityTableViewCell
        
        cell.imag.image = arrcities[indexPath.row]
        return cell 
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let photo = arrcities[indexPath.row]
        
        let photoSize = photo.size.width / photo.size.height
        return tableView.frame.width / photoSize
    }


}
