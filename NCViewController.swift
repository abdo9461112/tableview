//
//  NCViewController.swift
//  table view
//
//  Created by abdo emad  on 17/06/2023.
//

import UIKit

class NCViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var arrnumbers = [String]()
    let refrashCountrol = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.delegate = self
        refrashCountrol.tintColor = UIColor.red
        refrashCountrol.addTarget(self, action: #selector(getdata), for: .valueChanged)
        tableView.addSubview(refrashCountrol)

    }
    @objc func getdata(){
        arrnumbers.append("value\(arrnumbers.count)")
        refrashCountrol.endRefreshing()
        tableView.reloadData()
    }

  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrnumbers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "numbercells", for: indexPath)
        cell.textLabel?.text = arrnumbers[indexPath.row]
        return cell
    }
    
}
