//
//  userViewController.swift
//  table view
//
//  Created by abdo emad  on 14/06/2023.
//

import UIKit

class userViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var textusername: UITextField!
    
    var arryNames = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.delegate = self
        tableview.dataSource = self
        
        
    }
    
    @IBAction func butedit(_ sender: Any) {
        
        tableview.isEditing = !tableview.isEditing
    }
    @IBAction func btnadd(_ sender: Any) {
        
        let trimmedText = textusername.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
            
            if trimmedText.isEmpty {
              
            showAlert()
            } else {
                arryNames.append(trimmedText)
                let indx = IndexPath(row : arryNames.count - 1 , section : 0)
                tableview.beginUpdates()
                tableview.insertRows(at: [indx], with: .automatic)
                tableview.endUpdates()
                textusername.text = ""
                
            }
    
        }
    func showAlert() {
           
        let alert = UIAlertController(title: "", message: "please type something", preferredStyle: .alert)
        
            let okAction = UIAlertAction(title: "OK",style: .default,handler: { _ in
                    // Handle the OK button action if needed
                }
            )

            alert.addAction(okAction)

            present(alert, animated: true, completion: nil)
        }
   
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        textusername.sizeToFit()
//    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arryNames.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath)
        cell.textLabel?.text = arryNames[indexPath.row]
        cell.textLabel?.numberOfLines = 0
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        arryNames.swapAt(sourceIndexPath.row, destinationIndexPath.row)
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteAction = UIContextualAction(style: .destructive, title: "delete") { ( action , view, completionHandler) in
            self.arryNames.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            completionHandler(true)
        }
        
        let favoriteAction = UIContextualAction(style:.normal , title: "favorite" ) { (_, _, _) in
            print ("user add")
            
        }
        favoriteAction.image = UIImage(systemName: "heart")
        deleteAction.image = UIImage(systemName: "trash")
        favoriteAction.backgroundColor = UIColor(named: "red")
        return UISwipeActionsConfiguration(actions: [deleteAction ,favoriteAction])
    }
    
    
    
    
}
